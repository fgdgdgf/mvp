//
//  PatternsTableViewController.swift
//  MVX Patterns In Swift
//
//  Created by Yaroslav Voloshyn on 18/07/2017.
//

import UIKit

final class PatternsTableViewController: UITableViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     if let mvpViewController = segue.destination as? MVPViewController {
            let presenter = ConcretePresenter(view: mvpViewController)
            mvpViewController.presenter = presenter
        }

    }

}
